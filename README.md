# Portfolio Page Marcus Lindbloom

Quick and easy React Web Application to further build on in the future.

## Libraries used
- React.js 17

## Features

Nothing much really, have a look through my projects on the site instead. 

## Installing

'git clone https://gitlab.com/marcuslindbloom/lindbloom-portfolio'
To clone the project to your chosen folder.

'npm install'
Installs all the relevant packages to run the react app.

'npm start'
Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


## Deployment

'npm build' to start production build

## Versions

v.1.0 First production build

## Developers

Marcus Lindbloom(marcus.lindblom@me.com)
