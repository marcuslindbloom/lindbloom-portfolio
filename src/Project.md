Portfolio Project v.3


[] PUBLISH_ON_SURGE
[] DELETE THIS FILE

[] LAYOUT
	[] Components
		[X] React Router
		[] React Helmet	
	[] Responsiveness
	[X] Containers
		[X] Home
		[X] Footer
 	[X] Links
		[X] Gitlab
		[X] Instagram
		[X] Email
		[X] LinkedIn
	[] Incoporating projects
		[] Rainbow Beetroot
			[X] Story behind
			[X] Stack
			[X] Link w picture
			[] Gitlab Link
		[] MultiCast - Weather App
			[] Story behind
			[] Stack (Included APIs)
			[] Pictures 
			[] Gitlab Link
		[] Culinary Kitchen
			[] Story behind
			[] Stack
			[] Link w picture
			[] Gitlab Link


[X] STRUCTURE
	[X] I am
	[X] Experience
	[X] Work


[X] DESIGN
	[X] Colorscheme
		[X] ColorVariables in CSS
	[X] Fonts
	[X] Images / Icons


