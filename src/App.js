import React from 'react';
import Home from './containers/Home';
import Footer from './containers/Footer';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Helmet } from 'react-helmet';

function App() {
  return (
    <div>
      <Helmet>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta charset="UTF-8" />
        <meta name="keywords" content="Portfolio, Web development, Full-stack, Projects" />
        <meta name="author" content="Marcus Lindbloom" />
        <title>Marcus Lindbloom</title>

       {/* <!-- Google / Search Engine Tags -->*/}
        <meta itemprop="name" content="Marcus Lindbloom"/>
        <meta itemprop="description" content="Portfolio page of Marcus Lindbloom"/>
        <meta itemprop="image" content="https://media-exp1.licdn.com/dms/image/C4E03AQHJR2zAxwn7Ww/profile-displayphoto-shrink_200_200/0/1620069151695?e=1634774400&v=beta&t=AoyZFshyi0RT_uCKX7VpsKSxSWLy-EjvOC6ZupD5URk"/>

      {/*  <!-- Facebook Meta Tags -->*/}
        <meta property="og:url" content="http://marcuslindbloom.surge.sh"/>
        <meta property="og:type" content="website"/>
        <meta property="og:title" content="Marcus Lindbloom"/>
        <meta property="og:description" content="Portfolio page of Marcus Lindbloom"/>
        <meta property="og:image" content="https://media-exp1.licdn.com/dms/image/C4E03AQHJR2zAxwn7Ww/profile-displayphoto-shrink_200_200/0/1620069151695?e=1634774400&v=beta&t=AoyZFshyi0RT_uCKX7VpsKSxSWLy-EjvOC6ZupD5URk"/>

       {/* <!-- Twitter Meta Tags -->*/}
        <meta name="twitter:card" content="summary_large_image"/>
        <meta name="twitter:title" content="Marcus Lindbloom"/>
        <meta name="twitter:description" content="Portfolio page of Marcus Lindbloom"/>
        <meta name="twitter:image" content="https://media-exp1.licdn.com/dms/image/C4E03AQHJR2zAxwn7Ww/profile-displayphoto-shrink_200_200/0/1620069151695?e=1634774400&v=beta&t=AoyZFshyi0RT_uCKX7VpsKSxSWLy-EjvOC6ZupD5URk"/>
      </Helmet>
      <Router>
      <Route exact path="/" component={Home} />
      </Router>
      <Footer />
    </div>
  );
}

export default App;
