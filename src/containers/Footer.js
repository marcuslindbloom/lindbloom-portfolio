import React from 'react';
import {ReactComponent as IconLinkedIn} from '../static/images/linkedin.svg';
import {ReactComponent as IconEmail} from '../static/images/email.svg';
import {ReactComponent as IconInsta} from '../static/images/instagram.svg';
import {ReactComponent as IconGit} from '../static/images/gitlab.svg';

const Footer = () => {
	return(
		<>
			<div className="boxOverlapping">
				<h3>Open for work!</h3>
				<p> Looking for a junior developer or an intern?</p>
				<a href="mailto: marcus.lindblom@me.com" className="button"><h3 id="connectButton">Let's Connect</h3></a>
			</div>
			<div className="footer">
				<div className="socialFooter">
					<a href="https://www.linkedin.com/in/marcuslindbloom/"><IconLinkedIn className="iconFoot" alt="LinkedIn Icon" /></a>
					<a href="mailto: marcus.lindblom@me.com"><IconEmail className="iconFoot" alt="Email icon" /></a>
					<a href="https://www.instagram.com/marcus_lindbloom/"><IconInsta className="iconFoot" alt="Instagram icon"/></a>
					<a href="https://gitlab.com/marcuslindbloom"><IconGit className="iconFoot" alt="Gitlab Icon"/></a>		
				</div> 
				<p>&copy; Marcus Lindbloom 2021</p>
			</div>
		</>
	)
}
export default Footer;