import React from 'react'
import "../styles/Styles.css"

// Icons
import {ReactComponent as IconLaptop} from '../static/images/laptop.svg'
import {ReactComponent as IconCode} from '../static/images/code.svg'
import {ReactComponent as IconMale} from '../static/images/male.svg'
import {ReactComponent as IconGit} from '../static/images/gitlab.svg';

// Images
import ImgRB from '../static/images/RainbowBeetroot.png'
import ImgCK from '../static/images/CulinaryKitchen.png'
import ImgMC1 from '../static/images/MultiCast1.png'
import ImgMC2 from '../static/images/MultiCast2.png'


function Home() {
  


  return (
    <div className="container">
      {/*Navigation Bar*/}
      <div className="navBar">
        <div>
          <h4 className="logo">ML</h4>
        </div>  
        <div className="navBarRight">
          <a href="#iam"><h4 className="buttons">I AM</h4></a>
          <a href="#experience"><h4 className="buttons">EXPERIENCE</h4></a>
          <a href="#work"><h4 className="buttons">WORK</h4></a>
        </div>
      </div>
    {/*Box Container Alternating*/}
      <section className="section">
        <div className="boxContainerLeft">
          <p>Hello, I am</p>
          <h1>MARCUS LINDBLOOM</h1>
          <p>Aspiring Full-Stack web developer</p>
          <p>MongoDB | React | React Native | Express | CSS3 | HTML5 | JS </p>
        </div>
      </section>
      <section className="section" id="iam">
        <div className="boxContainerRight">
          <h2>I AM</h2>
          <p>Not just an allround great guy, but I can also guide you through almost any range of beers or drinks at the afterwork. Yea, been employed too long in the hospitality industry so it was time to change line of work to something I really enjoyed in the past. It's not writing introductory texts (thank god!), but web development.</p>
          <p>Recently graduated from Barcelona Coding Schools JavaScript Full-Stack Bootcamp, I'm fully charged to throw myself headfirst into programming, again. Maybe, I should tell you that I started programming when I was about 13 years, but Pascal and C is not that Hip these days. Also studied programming in secondary school, so learned the basics of Java, C++, PHP, HTML, CSS back then. Even though it was a long time ago when we were running linux, setting up our own local networks, developing games etc, it has really set a foundation of general computer knowledge. </p>   
          <p>What I learnt from working in the hospitality industry around Europe, leading teams of up to 180+ employees among other things, I work with no prestige and quickly adapt to new working cultures. So, have a look through my recent projects and contact me if you are interested in working together. </p>  
        </div>
      </section>
        {/*Standard Container*/}
       <section className="section" >
        <div className="standardContainer">
          <h2 className="h2Heading" id="experience">EXPERIENCE</h2>
        </div>  
       {/*Triple Column Grid*/}
        <div className="tripleColumnGrid">
          <div className="gridInnerWrapper">
            <IconLaptop alt="Laptop icon" className="iconPassive" />
            <h4>BUSINESS PROFESSIONAL</h4>
            <p>Business experience spanning from large international cooporations to small private companies.</p>
            <h5>AWARDS & NOMINATIONS</h5>
            <ul>
              <li>Awarded Best Food&Beverage Department Hilton C.Europe <br/>2018<br/><br/></li> 
              <li>Nominated F&B Leader of the Year Hilton C.Europe <br/>2018<br/></li>
            </ul>
            <h5>KEY SKILLS GAINED</h5>
            <ul>
              <li>Project management</li> 
              <li>Business analysis</li>
              <li>Operations planning and management</li>   
              <li>Budgeting/forecasting</li>
              <li>Employee training and development</li>
            </ul>
          </div>
          <div className="gridInnerWrapper">
            <IconCode alt="Code icon" className="iconPassive" />
            <h4>FULL-STACK DEVELOPER</h4>
            <p>This is my key focus moving forward. </p>
            <h5>COMPLETED COURSE</h5>
            <ul>
              <li><span>Barcelona Coding School:</span></li>  
              <li>JavaScript</li>
              <li>MongoDB</li>
              <li>Express</li>
              <li>NodeJs</li>
              <li>React Native</li>
              <li>HTML5 & CSS3</li>
              <li>Git<br/><br/></li>
            </ul>
            <h5>CURRENTLY STUDING:</h5>
            <ul>
               <li><span>Java - Udemy & CodeGym.cc:</span></li>
              <li>Java Data Structures & Algorithmns</li>
              <li>SQL</li>
              <li>Intro to CI & CD</li>
            </ul>
          </div>
          <div className="gridInnerWrapper">
            <IconMale alt="People icon" className="iconPassive" />
            <h4>EXPERIENCED LEADER</h4>
            <p>More than 10 years experience of leading teams and other leaders.</p>
            <h5>LEADERSHIP CORE</h5>
            <p>To lead poeple you need to understand what they do. </p>
            <h5>PERSONALITY TRAITS</h5>
            <ul>
              <li>Collaborative</li>
              <li>Humble</li>
              <li>Organized</li>
              <li>Careful</li>
              <li>Determined</li>
              <li>Goal-oriented</li>
              <li>Assertive</li>
              <li>Resilient</li>
              <li>Composed</li>
              <li>Even-tempered</li>
              <li>Curious</li>
              <li>Change oriented</li>
            </ul>
          </div>
        </div>    
      </section>
      {/*Splash Container*/}
      <section className="section" >
        <div className="splashContainer">
          <h2>BUSINESS PROFESSIONAL | FULL-STACK DEVELOPER | EXPERIENCED LEADER</h2>
        </div>      
      </section>
       {/*Standard Container*/}
      <section className="section" >
        <div className="standardContainer">
          <h2 className="h2Heading" id="work">WORK & PROJECTS</h2>
        </div>  
        <div className="boxContainerLeftSmall">
          <h2>RAINBOW BEETROOT</h2>
          <img src={ImgRB} alt="RainbowBeetroot Website" className="alternatingImg" />
          <p>Fictional grocery basket delivery with recipes.</p>
          <p style={{"fontWeight":"bold", "margin":0}}>Features:</p>
          <ul>
            <li>Argon2 encrypted Client registration and login</li>
            <li>Express server, MongoDB Atlas Database</li>
            <li>Stripe integration for payment</li> 
            <li>MailerServer/Client for contact form and confirmation emails</li>
          </ul>
          <p><span style={{"fontWeight":"bold"}}>Stack:</span> MongoDB | React | Express | CSS3 | HTML5 | JS </p>
          <a href="http://159.89.101.181"><p>To Rainbow Beetroot Website</p></a>
          <a href="https://gitlab.com/marcuslindbloom/rainbowbeetroot-delivery"><IconGit className="iconFoot" alt="Gitlab Icon"/></a>
        </div>
        <div className="boxContainerRightSmall">
          <h2>MULTICAST WEATHER APP</h2>
          <div className="doubleColumnGrid">
            <img src={ImgMC1} alt="Multicast Weather App" className="alternatingImg" />
            <img src={ImgMC2} alt="Multicast Weather App" className="alternatingImg" id="Multi2"/>
          </div>
          <p>Weather app forecasting from 3 sources</p>
          <p style={{"fontWeight":"bold", "margin":0}}>Features:</p>
          <ul>
            <li>Supporting Google Geolocation & Geocoding APIs</li>
            <li>Sourcing from OpenWeatherMap.org, Yr.no & Weatherapi.com</li>
            <li>Under submission to Google Play and App Store</li> 
          </ul>
          <p><span style={{"fontWeight":"bold"}}>Stack:</span> React Native | CSS3 | JS </p>
          <p>Test it out using Expo.io, available for download at GitLab</p>
          <a href="https://gitlab.com/marcuslindbloom/multicast-weather-application"><IconGit fill="white" className="iconFoot" alt="Gitlab Icon"/></a>
        </div>
        <div className="boxContainerLeftSmall" id="lastBox">
          <h2>CULINARY KITCHEN</h2>
          <img src={ImgCK} alt="Culinary Kitchen Website" className="alternatingImg" />
          <p>Fictional restaurant website.</p>
          <p style={{"fontWeight":"bold", "margin":0}}>Features:</p>
          <ul>
            <li>Grid & Flexbox</li>
            <li>Responsive on all platforms</li>
          </ul>
          <p><span style={{"fontWeight":"bold"}}>Stack:</span> HTML5 | CSS3 </p>
          <a href="http://culinary-kitchen.surge.sh/"><p>To Culinary Kitchen Website</p></a>
          <a href="https://gitlab.com/marcuslindbloom/culinary-kitchen"><IconGit className="iconFoot" alt="Gitlab Icon"/></a>
        </div>
      </section>
    </div>
  );
}

export default Home;
